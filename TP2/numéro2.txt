2- Écrire un algorithme qui lit un nombre entier positif entre 0 et 100  et détermine si c'est un nombre premier. (indice : essayer de diviser ce nombre par les nombres venant avant lui). Valider l’entrée.

E:
Un nombre

S:
Texte qui informe si le nombre est premier ou non

DI:
---

T:
Écrire entrer un nombre:
Lire le nombre
Si le nombre est plus grand ou égale à 0 et plus petit ou égale à 100 alors
Répéter ?

VARIABLES
    nombre : Entier (E)
CONSTANTES
    --
DÉBUT
    ÉCRIRE "Entrer un nombre: "
	LIRE nombre
	SI nombre >= 0 ET nombre <= 100 ALORS
	    RÉPETER ?